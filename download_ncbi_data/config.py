"""
NCBI RNAseq dataset downloader

"""

from os.path import split as pathsplit
from os.path import abspath


############ Variables #################################################
PROJECT_NAME_DICT = {"pancreas": ("pancreatic_circulating_tumor", None),
                     "waterfall": ("waterfall_neural_stemcells", None),
                     "kim": ("kim_renal_carcinoma", None),
                     "glio": ("glioblastoma", None),
                     "lung": ("lung_epithelium", 'GSE52583'),
                     "CTC": ("2015_human_CTC_prostate", None),
                     "tirosh": ("tirosh_2016_melanoma", None),
                     "scMTseq": ("hu_scMTseq_2016", "GSE49828"),
                     "angermueller": ("angermueller_scMandT_seq_2016", "GSE74535"),
                     "braune": ("braune_2016_breast_cancer", "GSE77308"),
                     "weterung": ("weterung_2016_organoid", "GSE65253"),
                     "treutlein": ("treutlein_2016_cerebral", "GSE75140"),
                     "chung": ("chung_notch_2016", "GSE75688"),
                     }

PROJECT_NAME, GSE = PROJECT_NAME_DICT["scMT_seq"]

PATH_DATA = "/data/opoirion/{0}/".format(PROJECT_NAME)
PATH_SOFT =  "{0}/{1}.soft".format(PATH_DATA, PROJECT_NAME)
NB_THREADS = 2 # number of threads to use for downloading rsa files
FASTQ_DUMP_OPTION = "-B"
VERBOSE = True
########################################################################
