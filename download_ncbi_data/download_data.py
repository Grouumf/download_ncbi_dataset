#! /usr/bin/python

"""
download data from NCBI according to GEO accession number
"""

from os import mkdir

from os.path import isdir
from os.path import isfile
from os import popen
from shutil import copyfile

import urllib2
import re

from distutils.dir_util import mkpath

from download_ncbi_data.config import PATH_SOFT
from download_ncbi_data.config import PATH_DATA
from download_ncbi_data.config import NB_THREADS
from download_ncbi_data.config import VERBOSE

from multiprocessing.pool import ThreadPool

from time import sleep


############ VARIABLES ############
PATH_SEQ = PATH_DATA + '/fastq/'
PATH_GZ = PATH_DATA + '/compressed_data/'

###################################


def main():
    download_data()

def _download(url):
    """ """
    gsm, address = url

    try:
        if address[-3:] != '.gz':
            srx = address.rsplit('/', 1)[-1]
            url = urllib2.urlopen(address).read().split()
            srr = url[-1]

            srr_url = "{0}/{1}/{1}.sra".format(address, srr)
            f_name = "{0}{1}__{2}__{3}.sra".format(PATH_SEQ,
                                                   gsm,
                                                   srx,
                                                   srr)
        else:
            srr_url = address
            f_name = "{0}{1}".format(PATH_GZ,
                                     address.rsplit('/', 1)[1])

            if not isdir(PATH_GZ):
                mkdir(PATH_GZ)

        if VERBOSE:
            print 'downloading: {0}'.format(srr_url)
    except Exception as e:
        print 'error with SRX {0}!!!'.format(address)
        return "{1} {0}".format(str(e), address)

    if isfile(f_name):
        print "{0} already exists continue...".format(f_name)
        return "{0} already exists continue...".format(f_name)

    try:
        if VERBOSE:
            verbose = ''
        else:
            verbose = '--no-verbose'
        cmd = "wget {2} -O {0} {1} ".format(f_name, srr_url, verbose)

        res = popen(cmd).read()
        print '{0} successfully downloaded'.format(f_name)
        return

    except Exception as e:
        print 'error while downloading {0}!!!'.format(address)
        return "{1} {0}\n".format(str(e), address)

    sleep(0.2)

def download_data():
    """download dataset from ncbi """

    urls = get_urls()

    if not isdir(PATH_SEQ):
        mkpath(PATH_SEQ)

    f_srr_srx = open(PATH_DATA + "srr_srx.csv", "w")
    f_error = open(PATH_DATA + "error_log.txt", "w")

    thread_list = []

    thread_pool = ThreadPool(processes=NB_THREADS)

    res = thread_pool.map(_download, urls)

    print "######## errors founds:"
    for err in res:
        if err:
            print err

def get_urls():
    """
    get download addresses as GSM id according to the following template:
    169. TuMP2-10b
    Organism:	Mus musculus
    Source name:	mouse pancreatic tumor
    Platform: GPL15907 Series: GSE51372
    FTP download: SRA SRX364871
                            ftp://ftp-trace.ncbi.nlm.nih.gov/
                            sra/sra-instant/reads/ByExp/sra/SRX/SRX364/SRX364871/
    Sample		Accession: GSM1243834	ID: 301243834
    """
    regex_url = re.compile("(?<=!Sample_supplementary_file_1 = )ftp://.+(?=\n)")
    regex_gsm = re.compile("(?<=\^SAMPLE = )GSM[0-9]+")

    read = open(PATH_SOFT, 'r').read()

    addresses = regex_url.findall(read)
    gsms = regex_gsm.findall(read)

    return zip(gsms, addresses)

if __name__ == "__main__":
    main()
