# SNV computation pipeline

## installation (local)

```bash
pip install -r requirements.txt --user
```

## configuration
* all local variables must be set into the file config.py

```python
"""
NCBI RNAseq dataset downloader

the summary file should contain data info following this template:
    169. TuMP2-10b
    Organism:	Mus musculus
    Source name:	mouse pancreatic tumor
    Platform: GPL15907 Series: GSE51372
    FTP download: SRA SRX364871
                            ftp://ftp-trace.ncbi.nlm.nih.gov/
                            sra/sra-instant/reads/ByExp/sra/SRX/SRX364/SRX364871/
    Sample		Accession: GSM1243834	ID: 301243834
"""

############ Variables #################################################
PATH_LINK = "/home/opoirion/code/download-ncbi-data/" \
            "/data/waterfall_dataset_summary.txt"
PATH_DATA = "/data/opoirion/waterfall_neural_stemcells/"
########################################################################
```

## usage
* download and extract data:
```bash
python eSNV_pipeline/download_ncbi_data/download_data.py
```
