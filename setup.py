from setuptools import setup, find_packages
import sys, os

VERSION = '0.1.2'

setup(name='download_ncbi_dataset',
      version=VERSION,
      description="eSNV distance",
      long_description="""""",
      classifiers=[],
      keywords='ncbi download rna seq',
      author='o_poirion',
      author_email='o.poirion@gmail.com',
      url='',
      license='MIT',
      packages=find_packages(exclude=['examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[],
      )
